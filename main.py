# ----------------------------------
# General Instruction Set assembler
# JISA v2.1
# Copyright (c) 2015, John Allen
# ----------------------------------
#
# Licenced under GPL v3.0
# See LICENCE for more licencing details
#
__author__ = 'John'
import yaml
import argparse
import string
import os
import sys


def main():
    parser = argparse.ArgumentParser(description='Assemble your IS')
    parser.add_argument('i', help='input file')
    parser.add_argument('o', help='output file')
    parser.add_argument('--otype', help='<hex|bin> hexidecimal or binary output (Default binary)', default='bin')
    parser.add_argument('-s', help='Instruction set .yaml location', default='IS.yaml')
    parser.add_argument('-b', help='Number of black lines at the start of your file', default=0, type=int)
    parser.add_argument('-p', help='Position of the opcode within the instruction', default=0, type=int)
    argv = parser.parse_args()
    imp_name = ['form', 'inst', 'args']
    i = 0
    d = {}
    for j in yaml.load_all(open(argv.s).read()):
        d[imp_name[i]] = j
        i += 1
    form = d['form']
    inst = d['inst']
    args = d['args']
    sour = open(argv.i, 'r')
    source = sour.read()
    sour.close()
    source = lex(source)
    data = parse(source, form, inst, args, argv.b, argv.p, argv.otype)
    save(argv.o, data)
    print('Assemble completed with no errors')


def lex(source):
    source = source.split("\n")  # split every line into separate entry
    tag = []
    tags = []
    for i in source:  # split every entry by spaces
        tag.append(i.split(" "))
    for j in tag:  # filter out comments
        loc = None
        for k in j:
            if k.startswith("#"):
                loc = j.index(k)
            else:
                loc = None
        loc = j[0:loc]
        tags.append(loc)  # generate list of filtered results
    return tags


def parse(source, form, inst, cust, addr_line, op_pos, rend_type):
    addr_dict = {'start': '0'}
    err = []
    ans = []
    source_line = 1
    for line in source:
        if line[0].startswith('@'):
            addr_dict[line[0][1:]] = addr_line
        else:
            length = inst[line[op_pos]][0].split(';')[1:]
            length = [i.split(',') for i in length]
            length = [int(i) for i in length[0]]
            addr_line += len(length)
    for line in source:
        if line[0].startswith('@') is False:
            if line[op_pos] in inst:
                if inst[line[op_pos]][0] in form:
                    lform = form[inst[line[op_pos]][0]]
                    args = inst[line[op_pos]][1:]
                    type_dict = {x.split(';')[0]: [x.split(';')[1], args.index(x)] for x in args}
                    out = subparse(line, lform, type_dict, cust, addr_dict)
                    result = out[0]
                    err_l = out[1]
                    l_total = out[2]
                    if l_total < len(line):
                        err_l.append('argl_err')
                    i = 0
                    length = inst[line[op_pos]][0].split(';')[1:]
                    length = [i.split(',') for i in length]
                    length = [int(i) for i in length[0]]
                    for x in length:
                        ans.append(result[i:(i + x)].zfill(x))
                        i += x
                        addr_line += 1
                else:
                    err.append(err_hand(source_line, 'form_err'))
            else:
                err.append(err_hand(source_line, 'op_err'))
            err += [err_hand(source_line, x) for x in err_l]
        source_line += 1
    if err:
        print('\n'.join(err))
        sys.exit(1)
    else:
        if rend_type == 'bin':
            return bin_r(ans)
        elif rend_type == 'hex':
            return hex_r(ans)


def subparse(line, lform, type_dict, cust, addr_dict):
    i = 1
    l_total = 0
    result = ''
    err = []
    for x in type_dict:
        if x.startswith('?'):
            if is_type(line[int(type_dict[x][1])], type_dict[x][0], cust) is False:
                for q in type_dict:
                    if int(type_dict[q][1]) > int(type_dict[x][1]):
                        type_dict[q][1] = str(int(type_dict[q][1]) - 1)
    while i in lform:
        current = lform[i].split(';')
        el_length = current[1]
        if current[0].startswith('?'):
            temp = is_type(line[int(type_dict[current[0]][1])], type_dict[current[0]][0], cust)
            if temp:
                result += temp
                i += 1
                l_total += 1
            elif temp is False:
                result += '0' * int(el_length)
                i += 1
        else:
            if current[0] in type_dict:
                if len(line) > int(type_dict[current[0]][1]):
                    if len(type_dict[current[0]][0].split(',')) >= 2:
                        temp = False
                        for el in type_dict[current[0]][0].split(','):
                            if is_type(line[int(type_dict[current[0]][1])], el, cust):
                                el_temp = type_dict[current[0]][0]
                                type_dict[current[0]][0] = el
                                outp = subparse(line, lform[current[0]][el], type_dict, cust, addr_dict)
                                result += outp[0]
                                err += outp[1]
                                l_total += outp[2]
                                type_dict[current[0]][0] = el_temp
                                temp = True
                        if temp is False:
                            err.append('case_err')
                    if type_dict[current[0]][0] == 'r':
                        if is_type(line[int(type_dict[current[0]][1])], 'r', cust):
                            temp = bin(int(line[int(type_dict[current[0]][1])].split('r')[1]))[2:]
                            if len(temp) <= int(el_length):
                                result += temp.zfill(int(el_length))
                                l_total += 1
                            else:
                                err.append('regl_err')
                        else:
                            err.append('reg_err')
                    elif type_dict[current[0]][0] == 'i':
                        if is_type(line[int(type_dict[current[0]][1])], 'i', cust):
                            temp = is_imm(line[int(type_dict[current[0]][1])])
                            if len(temp) <= int(el_length):
                                result += temp.zfill(int(el_length))
                                l_total += 1
                            else:
                                err.append('imml_err')
                        else:
                            err.append('imm_err')
                    elif type_dict[current[0]][0] == 'addr':
                        if is_type(line[int(type_dict[current[0]][1])], 'i', cust):
                            temp = is_imm(line[int(type_dict[current[0]][1])])
                        elif line[int(type_dict[current[0]][1])].startswith('$'):
                            if line[int(type_dict[current[0]][1])].split('&')[1] in addr_dict:
                                temp = is_imm(str(addr_dict[line[int(type_dict[current[0]][1])]]))
                        else:
                            temp = False
                        if temp:
                            if len(temp) <= el_length:
                                result += temp
                                l_total += 1
                            else:
                                err.append('addrl_err')
                        else:
                            err.append('addr_err')
                    elif current[0] == 'op':
                        result += type_dict[current[0]][0]
                        l_total += 1
                    elif type_dict[current[0]][0].startswith('~'):
                        if is_type(line[int(type_dict[current[0]][1])], type_dict[current[0]][0], cust):
                            result += cust[current[0]][line[int(type_dict[current[0]][1])]]
                            l_total += 1
                        else:
                            err.append('cust_err')
                    i += 1
                else:
                    err.append('args_err')
                    i += 1
            elif current[0] == '0':
                result += '0' * int(el_length)
                i += 1
            elif current[0] == '1':
                result += '1' * int(el_length)
                i += 1
            else:
                print('well shit...')
                i += 1
    return [result, err, l_total]


def save(resp, data):  # saves file
    file = open(resp, "w")
    file.write(data)
    file.close()


def hex_r(code):
    code = [bth(i) for i in code]
    code = "\n".join(code)  # compiles results to string
    if os.path.isfile('LICENCE.txt'):
        return code
    else:
        return 'Missing LICENCE.txt'


def bin_r(code):
    code = "\n".join(code)  # compiles results to string
    if os.path.isfile('LICENCE.txt'):
        return code
    else:
        return 'Missing LICENCE.txt'


def bth(binar):
    hex_v = hex(int(binar, 2))[2:]  # convertion to hex
    return hex_v


def is_type(check, test, args):
    if test == 'r':
        if check.startswith('r') and check.split('r')[1].isdigit():
            return bin(int(check.split('r')[1]))
    elif test == 'i':
        return is_imm(check)
    elif test.startswith('~'):
        if test.split('~')[1] in args and check in args[test.split('~')[1]]:
            return args[test.split('~')[1]][check]
    return False


def err_hand(line, err):
    if err == 'op_err':
        result = 'op is not defined in the config file'
    elif err == 'form_err':
        result = 'Format is not defined in the config file'
    elif err == 'args_err':
        result = 'Too few arguments provided'
    elif err == 'argl_err':
        result = 'Too many arguments provided'
    elif err == 'cust_err':
        result = 'Custom argument is undefined'
    elif err == 'reg_err':
        result = 'Invalid register argument provided'
    elif err == 'regl_err':
        result = 'Register value to large for provided bitwidth'
    elif err == 'imm_err':
        result = 'Invalid immediate provided'
    elif err == 'imml_err':
        result = 'Immediate value to large for provided bitwidth'
    elif err == 'addr_err':
        result = 'Invalid address provided'
    elif err == 'addrl_err':
        result = 'Address vlaue too large for provided bitwidth'
    elif err == 'case_err':
        result = 'Unhandled argument in variable argument type'
    else:
        result = 'Unknown error'
    return 'Line ' + str(line) + ': ' + result


def is_imm(val):
    if val.startswith('0x') or val.startswith('0X'):
        if ishex(val[2:]):
            return bin(int(val[2:], 16))[2:]
    elif isbin(val):
        return val
    elif val.isdigit():
        return bin(int(val))[2:]
    else:
        return False


def ishex(numb):
    if all(d in string.hexdigits for d in numb):
        return True


def isbin(numb):  # checks if the number is binary
    for i in numb:
        if i == "0" or i == "1":
            pass
        else:
            return False
    return True


if __name__ == '__main__':
    main()