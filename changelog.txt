v1.*
2015-02-06: v1.0 First upload
2015-02-07: Error handler added
2015-02-08: Error handling changes and some changes to the var format
2015-02-08: Added command line argument handling and output options

v2.*
2015-02-22: v2.0 First upload
    - Overhaul on .yaml format
    - Added the ability to have optional arguments
    - Added line addressing
2015-02-23: Fixed issues with out of order arguments in the bit layout
2015-02-25: Fixed the inability to address a tag after the current program line
2015-02-25: Added the verification of the number of arguments required in the line